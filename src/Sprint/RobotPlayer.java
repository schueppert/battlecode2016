package sprint;

import battlecode.common.*;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;

public class RobotPlayer {
    //Friends
    static RobotController rc;
    static RobotInfo mothership;
    static final int ALAMO = 400;
    //Enemies
    static RobotInfo[] enemiesSR;
    static RobotInfo[] enemiesAR;
    static MapLocation nearestEnemy;
    static MapLocation weakestEnemy;
    static MapLocation rangedTarget;
    static MapLocation rallyPoint;
    static MapLocation nearestZombieDen;
    //Collections
    static HashMap<Integer, HashMap> friendlyArmy = new HashMap<Integer, HashMap>();
    static HashMap<Integer, HashMap> enemyArmy = new HashMap<Integer, HashMap>();
    //Maps and Navigation
    static Direction[][] points = new Direction[8][8];
    static Direction[] directions = Arrays.copyOfRange(Direction.values(), 0, 8);
    static final int[] PREFERENCE = {0, 1, -1, 2, -2, 3, -3, 4};
    static Direction orientation;


    public static void run(RobotController r) throws GameActionException {
        rc = r;
        init();
        while (true) {
            update();
            if (Clock.getBytecodesLeft() < 2000) Clock.yield();
            switch (rc.getType()) {
                case ARCHON:
                    if (enemiesSR.length > 0) move(rc.getLocation().directionTo(nearestEnemy).opposite());
                    build();
                    if (rallyPoint != null) move(rc.getLocation().directionTo(rallyPoint));
                    else move(orientation);
                    repair();
                    if (rc.getRoundNum() % 10 == 0) rebroadcastSurvey();
                    break;
                case SCOUT:
                    if (enemiesSR.length > 0) move(rc.getLocation().directionTo(nearestEnemy).opposite());
                    else if (rc.getRoundNum() > ALAMO & rallyPoint != null) move(rc.getLocation().directionTo(rallyPoint));
                    else move(orientation);
                    if (rc.getRoundNum() % 10 == 0) rebroadcastSurvey();
                    break;
                case SOLDIER:
                    if (enemiesAR.length > 0) attack();
                    if (enemiesSR.length > 0) move(rc.getLocation().directionTo(nearestEnemy));
                    else if (rc.getRoundNum() > ALAMO && rallyPoint != null) {
                        int range = rc.getLocation().distanceSquaredTo(rallyPoint);
                        if (range > 10) move(rc.getLocation().directionTo(rallyPoint));
                        else move(rc.getLocation().directionTo(rallyPoint).opposite());

                    } else if (nearestZombieDen != null) move(rc.getLocation().directionTo(nearestZombieDen));
                    else move(orientation);
                    break;
                case TURRET:
                    if (enemiesAR.length > 0) attack();
                    else if (rangedTarget != null && rc.canAttackLocation(rangedTarget) && rc.isWeaponReady())
                        rc.attackLocation(rangedTarget);
                    break;
            }
            if (!rc.isInfected() && rc.getHealth() < 20) rc.disintegrate();
            Clock.yield();
        }
    }

    static void init() throws GameActionException {
        if (rc.getType() == RobotType.ARCHON) mothership = rc.senseRobot(rc.getID());
        else {
            RobotInfo[] friends = rc.senseNearbyRobots(rc.getType().sensorRadiusSquared, rc.getTeam());
            for (RobotInfo f : friends) {
                if (f.type == RobotType.ARCHON) mothership = f;
                break;
            }
        }
        putUnit(mothership);
        makePoints();
        orientation = points[0][rc.getID() % 8];
        rallyPoint = rallyPoint();
        nearestZombieDen = nearest(enemyArmy, RobotType.ZOMBIEDEN);
    }

    static void update() throws GameActionException {
        rangedTarget = null;
        receiveSignals();
        if (rc.canSenseRobot(mothership.ID)) mothership = rc.senseRobot(mothership.ID);
        enemiesAR = rc.senseHostileRobots(rc.getLocation(), rc.getType().attackRadiusSquared);
        enemiesSR = rc.senseHostileRobots(rc.getLocation(), rc.getType().sensorRadiusSquared);
        if (enemiesSR.length > 0) {
            weakestEnemy = enemiesSR[0].location;
            double weakestHealth = enemiesSR[0].health;
            nearestEnemy = enemiesSR[0].location;
            for (RobotInfo e : enemiesSR) {
                putUnit(e);
                if (rc.getType() == RobotType.SCOUT || rc.getType() == RobotType.ARCHON) sendUnit(e);
                if (rc.getLocation().distanceSquaredTo(e.location) < rc.getLocation().distanceSquaredTo(nearestEnemy))
                    nearestEnemy = e.location;
                if (e.health < weakestHealth) {
                    weakestEnemy = e.location;
                    weakestHealth = e.health;
                }
            }
        }
        if (rc.getRoundNum() % 20 == 0) {
            rallyPoint = rallyPoint();
            nearestZombieDen = nearest(enemyArmy, RobotType.ZOMBIEDEN);
        }
        if (rc.getType() == RobotType.SCOUT) {
            RobotInfo[] friends = rc.senseNearbyRobots(rc.getType().sensorRadiusSquared, rc.getTeam());
            for (RobotInfo f : friends) putUnit(f);
        }
    }


    static MapLocation rallyPoint() {
        if (rc.getRoundNum() > ALAMO) return rallyPoint;
        int dx = 0;
        int dy = 0;
        Collection<MapLocation> archons = friendlyArmy.get(RobotType.ARCHON.ordinal()).values();
        for (MapLocation archon : archons) {
            dx += archon.x;
            dy += archon.y;
        }
        dx /= archons.size();
        dy /= archons.size();
        rallyPoint = new MapLocation(dx, dy);
        return rallyPoint;
    }

    static MapLocation nearest(HashMap<Integer, HashMap> army, RobotType type) {
        if (army.containsKey(type.ordinal())) {
            Collection<MapLocation> units = army.get(type.ordinal()).values();
            MapLocation nearest = (MapLocation) units.iterator().next();
            for (MapLocation unit : units)
                if (rc.getLocation().distanceSquaredTo(unit) < rc.getLocation().distanceSquaredTo(nearest))
                    nearest = unit;
            return nearest;
        } else return null;
    }

    static void attack() throws GameActionException {
        if (!(rc.isWeaponReady() && rc.isCoreReady())) return;
        if (!rc.canAttackLocation(weakestEnemy)) return;
        rc.attackLocation(weakestEnemy);
    }

    static void build() throws GameActionException {
        if (!rc.isCoreReady()) return;
        if (rc.getRoundNum() == 0) safeBuild(RobotType.SCOUT);
        else if (rc.getRoundNum() < ALAMO) safeBuild(RobotType.SOLDIER);
        else if (rc.getLocation().distanceSquaredTo(rallyPoint) < 20) safeBuild(RobotType.TURRET);
        else if (rc.getTeamParts() > 125) safeBuild(RobotType.SOLDIER);
    }

    static void safeBuild(RobotType type) throws GameActionException {
        Direction target = points[0][rc.getID() % 8];
        if (target.ordinal() > 8) return;
        for (Direction d : points[target.ordinal()])
            if (rc.canBuild(d, type) && rc.isCoreReady()) {
                rc.build(d, type);
                break;
            }
    }

    static void move(Direction target) throws GameActionException {
        if (enemiesSR.length > 0) combatMove(target);
        else safeMove(target);
    }

    static void safeMove(Direction target) throws GameActionException {
        if (target.ordinal() > 7) return;
        for (Direction d : points[target.ordinal()]) {
            if (turretCovered(rc.getLocation().add(d))) continue;
            if (rc.canMove(d) && rc.isCoreReady()) {
                orientation = d;
                rc.move(d);
                break;
            }
        }
    }

    static boolean turretCovered(MapLocation location) {
        if (enemyArmy.containsKey(RobotType.TURRET.ordinal()))
            for (MapLocation turret : (Collection<MapLocation>) enemyArmy.get(RobotType.TURRET.ordinal()).values()) {
                if (location.distanceSquaredTo(turret) <= RobotType.TURRET.attackRadiusSquared)
                    return true;
            }
        return false;
    }

    static void combatMove(Direction target) throws GameActionException {
        if (target.ordinal() > 7) return;
        Direction bestMove = Direction.NONE;
        int bestScore = combatScore(bestMove) + 1;
        for (Direction d : points[target.ordinal()]) {
            if (!rc.canMove(d)) continue;
            int score = combatScore(d);
            if (score < bestScore) {
                bestScore = score;
                bestMove = d;
            }
        }
        safeMove(bestMove);
    }

    static int combatScore(Direction d) {
        int score = 0;
        MapLocation newLocation = rc.getLocation().add(d.dx, d.dy);
        for (RobotInfo e : enemiesSR) {
            if (e.type == RobotType.ARCHON || e.type == RobotType.SCOUT) continue;
            if (newLocation.distanceSquaredTo(e.location) <= e.type.attackRadiusSquared) score += 1;
        }
        return score;
    }

    static void rebroadcastSurvey() throws GameActionException {
        rebroadcast(rc.getTeam(), RobotType.ARCHON);
        rebroadcast(rc.getTeam().opponent(), RobotType.TURRET);
    }

    static void rebroadcast(Team team, RobotType type) throws GameActionException {
        HashMap<Integer, HashMap> army = (team == rc.getTeam()) ? friendlyArmy : enemyArmy;
        if (army.containsKey(type.ordinal())) {
            HashMap units = army.get(type.ordinal());
            for (int id : (Collection<Integer>) units.keySet())
                sendUnit(id, (MapLocation) units.get(id), type.ordinal(), team);
        }
    }

    static void repair() throws GameActionException {
        for (RobotInfo f : rc.senseNearbyRobots(rc.getType().sensorRadiusSquared, rc.getTeam())) {
            if (f.type == RobotType.ARCHON) continue;
            if (f.health == f.type.maxHealth) continue;
            orientation = rc.getLocation().directionTo(f.location);
            if (rc.getLocation().distanceSquaredTo(f.location) < rc.getType().attackRadiusSquared)
                rc.repair(f.location);
            break;
        }
    }

    static void makePoints() {
        int handedness = (rc.getID() % 2 == 0) ? 1 : -1;
        for (int i = 0; i < directions.length; i++)
            for (int j = 0; j < PREFERENCE.length; j++) points[i][j] = directions[(i + PREFERENCE[j] * handedness + 8) % 8];
    }

    enum MessageType {
        EMPTY, ENEMY_UNIT, FRIENDLY_UNIT, SURVEY_DATA
    }

    static void sendUnit(RobotInfo e) throws GameActionException {
        sendUnit(e.ID % 256, e.location, e.type.ordinal(), e.team);
    }

    static void sendUnit(int id, MapLocation location, int type, Team team) throws GameActionException {
        if (rc.getMessageSignalCount() >= GameConstants.MESSAGE_SIGNALS_PER_TURN) return;
        int message = (id % 256) * 256 * 256 * 128;
        message += (location.x - rc.getLocation().x + 128) * 256 * 128;
        message += (location.y - rc.getLocation().y + 128) * 128;
        message += type * 8;
        if (team == rc.getTeam()) message += MessageType.FRIENDLY_UNIT.ordinal();
        else message += MessageType.ENEMY_UNIT.ordinal();
        rc.broadcastMessageSignal(message, 0, Math.min(100, rc.getLocation().distanceSquaredTo(mothership.location)));
    }

    static void putUnit(RobotInfo e) {
        putUnit(e.ID % 256, e.location, e.type.ordinal(), e.team);
    }

    static void putUnit(int id, MapLocation location, int type, Team team) {
        HashMap<Integer, HashMap> army = (team == rc.getTeam()) ? friendlyArmy : enemyArmy;
        HashMap units = (army.containsKey(type)) ? army.get(type) : new HashMap<Integer, MapLocation>();
        units.put(id, location);
        army.put(type, units);
    }

    static void putMessage(Team team, int message, Signal signal) {
        int id = message / 256 / 256 / 128;
        int dx = (message / 256 / 128) % 256 + signal.getLocation().x - 128;
        int dy = (message / 128) % 256 + signal.getLocation().y - 128;
        MapLocation location = new MapLocation(dx, dy);
        int type = (message / 8) % 16;
        putUnit(id, location, type, team);
        if (rc.getType() == RobotType.TURRET && team == rc.getTeam().opponent())
            if (rangedTarget == null || rc.getLocation().distanceSquaredTo(location) < rc.getLocation().distanceSquaredTo(rangedTarget))
                rangedTarget = location;
    }

    static void receiveSignals() {
        Signal[] signals = rc.emptySignalQueue();
        for (Signal signal : signals) {
            if (signal.getTeam() != rc.getTeam()) continue; //TODO spying!
            for (int message : signal.getMessage())
                switch (MessageType.values()[message % 8]) {
                    case EMPTY:
                        break;
                    case ENEMY_UNIT:
                        putMessage(rc.getTeam().opponent(), message, signal);
                        break;
                    case FRIENDLY_UNIT:
                        putMessage(rc.getTeam(), message, signal);
                        break;
                    case SURVEY_DATA:
                        break;
                }
        }
    }

}
