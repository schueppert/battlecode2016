package fireants1;

import battlecode.common.*;

import java.util.Arrays;
import java.util.HashMap;

public class RobotPlayer {
    //Friends
    static RobotController rc;
    static RobotInfo mothership;
    static RobotInfo[] friends;
    //Enemeies
    static RobotInfo[] enemiesSR;
    static RobotInfo[] enemiesAR;
    static RobotInfo nearestEnemy;
    static RobotInfo weakestEnemy;
    static MapLocation nearestEnemyArchon;
    static MapLocation rangedTarget;
    //Maps and Navigateion
    static Direction[][] points = new Direction[8][8];
    static Direction[] directions = Arrays.copyOfRange(Direction.values(), 0, 8);
    static final int[] PREFERENCE = {0, 1, -1, 2, -2, 3, -3, 4};
    static Direction orientation;
    static MapLocation origin;
    static HashMap<Integer, Integer> surveyData = new HashMap<Integer, Integer>();


    public static void run(RobotController r) throws GameActionException {
        rc = r;
        init();
        while (true) {
            update();
            switch (rc.getType()) {
                case ARCHON:
                    if (enemiesSR.length > 0) move(rc.getLocation().directionTo(nearestEnemy.location).opposite());
                    build();
                    if (nearestEnemyArchon != null) move(rc.getLocation().directionTo(nearestEnemyArchon));
                    repair();
                    break;
                case SCOUT:
                    if (nearestEnemyArchon == null) move(orientation);
                    else move(rc.getLocation().directionTo(mothership.location));
                    if (rc.getRoundNum() % 10 == 0) survey();
                    break;
                case SOLDIER:
                    if (enemiesAR.length > 0) attack();
                    if (enemiesSR.length > 0) combatMove(rc.getLocation().directionTo(nearestEnemy.location));
                    else if (nearestEnemyArchon == null) safeMove(orientation);
                    else safeMove(rc.getLocation().directionTo(mothership.location));
                    break;
                case TURRET:
                    //attack();
                    break;
            }
            String[] indicators = new String[3];
            indicators[0] = (nearestEnemyArchon != null) ? nearestEnemyArchon.toString() : "null";
            indicators[1] = (nearestEnemy != null) ? nearestEnemy.location.toString() : "null";
            indicators[2] = (weakestEnemy != null) ? weakestEnemy.location.toString() : "null";
            rc.setIndicatorString(0, "Enemy Archon: " + indicators[0]);
            rc.setIndicatorString(1, "Nearest Enemy: " + indicators[1]);
            rc.setIndicatorString(2, "Weakest Enemy: " + indicators[2]);
            Clock.yield();
        }
    }

    static void init() throws GameActionException {
        if (rc.getType() == RobotType.ARCHON) mothership = rc.senseRobot(rc.getID());
        else {
            friends = rc.senseNearbyRobots(rc.getType().sensorRadiusSquared, rc.getTeam());
            for (RobotInfo f : friends) {
                if (f.type == RobotType.ARCHON) mothership = f;
                break;
            }
        }
        origin = mothership.location;
        makePoints();
        orientation = points[0][rc.getID() % 8];
    }

    static void update() throws GameActionException {
        receiveSignals();
        friends = rc.senseNearbyRobots(rc.getType().sensorRadiusSquared, rc.getTeam());
        if (rc.canSenseRobot(mothership.ID)) mothership = rc.senseRobot(mothership.ID);
        enemiesAR = rc.senseHostileRobots(rc.getLocation(), rc.getType().attackRadiusSquared);
        if (enemiesAR.length > 0) {
            weakestEnemy = enemiesAR[0];
            for (RobotInfo e : enemiesAR) {
                if (e.type == RobotType.ZOMBIEDEN) continue;
                if (e.health < weakestEnemy.health || weakestEnemy.type == RobotType.SCOUT || e.type == RobotType.VIPER) weakestEnemy = e;
            }
        }
        enemiesSR = rc.senseHostileRobots(rc.getLocation(), rc.getType().sensorRadiusSquared);
        if (enemiesSR.length > 0) {
            nearestEnemy = enemiesSR[0];
            for (RobotInfo e : enemiesSR) {
                if (e.type == RobotType.ZOMBIEDEN) continue;
                if (rc.getLocation().distanceSquaredTo(e.location) < rc.getLocation().distanceSquaredTo(nearestEnemy.location))
                    nearestEnemy = e;
            }
        }
    }

    static void attack() throws GameActionException {
        if (!(rc.isWeaponReady() && rc.isCoreReady())) return;
        if (!rc.canAttackLocation(weakestEnemy.location)) return;
        rc.attackLocation(weakestEnemy.location);
        //TODO Turret ranged attack
    }

    static void build() throws GameActionException {
        if (!rc.isCoreReady()) return;
        if (rc.getRoundNum() == 0) safeBuild(RobotType.SCOUT);
        else if (false) safeBuild(RobotType.TURRET);//TODO building Turrets
        else safeBuild(RobotType.SOLDIER);
    }

    static void safeBuild(RobotType type) throws GameActionException {
        Direction target = Direction.NORTH;
        if (nearestEnemyArchon != null) target = rc.getLocation().directionTo(nearestEnemyArchon);
        if (target.ordinal() > 8) return;
        for (Direction d : points[target.ordinal()])
            if (rc.canBuild(d, type) && rc.isCoreReady()) {
                rc.build(d, type);
                break;
            }
    }

    static void move(Direction target) throws GameActionException {
        if (enemiesSR.length > 0) combatMove(target);
        else safeMove(target);
    }

    static void safeMove(Direction target) throws GameActionException {
        if (target.ordinal() > 8) return;
        for (Direction d : points[target.ordinal()]) {
            if (rc.canMove(d) && rc.isCoreReady()) {
                orientation = d;
                rc.move(d);
                break;
            }
        }
    }

    static void combatMove(Direction target) throws GameActionException {
        if (target.ordinal() > 8) return;
        Direction bestMove = Direction.NONE;
        int bestScore = combatScore(bestMove);
        for (Direction d : points[target.ordinal()]) {
            if (!rc.canMove(d)) continue;
            int score = combatScore(d);
            if (score < bestScore) {
                bestScore = score;
                bestMove = d;
            }
        }
        if (rc.canMove(bestMove) && rc.isCoreReady()) {
            orientation = bestMove;
            rc.move(bestMove);
        }
    }

    static int combatScore(Direction d) {
        int score = 0;
        MapLocation newLocation = rc.getLocation().add(d.dx, d.dy);
        for (RobotInfo e : enemiesSR) {
            if (e.type == RobotType.ARCHON || e.type == RobotType.SCOUT) continue;
            if (newLocation.distanceSquaredTo(e.location) <= e.type.attackRadiusSquared) score += 1;
        }
        return score;
    }

    static void survey() throws GameActionException {
        if (enemiesSR.length > 0) for (RobotInfo e : enemiesSR)
            if (e.type == RobotType.ARCHON) {
                nearestEnemyArchon = e.location;
            }
        if (nearestEnemyArchon != null) sendSignal(mothership, MessageType.ENEMY_ARCHON, nearestEnemyArchon, 0);
    }

    static void repair() throws GameActionException {
        for (RobotInfo f : friends) {
            if (f.type == RobotType.ARCHON) continue;
            if (rc.getLocation().distanceSquaredTo(f.location) > rc.getType().attackRadiusSquared) continue;
            if (f.health == f.type.maxHealth) continue;
            rc.repair(f.location);
            break;
        }

    }

    static void makePoints() {
        int handedness = (rc.getID() % 2 == 0) ? 1 : -1;
        for (int i = 0; i < directions.length; i++)
            for (int j = 0; j < PREFERENCE.length; j++)
                points[i][j] = directions[(i + PREFERENCE[j] * handedness + 8) % 8];
    }

    enum MessageType {ENEMY_ARCHON, RANGED_TARGET, SURVEY_DATA}

    static void sendSignal(RobotInfo target, MessageType type, MapLocation location, int message2) throws GameActionException {
        int message1 = type.ordinal();
        message1 = message1 * 200 + target.ID % 200;
        message1 = message1 * 200 + location.x - origin.x + 100;
        message1 = message1 * 200 + location.y - origin.y + 100;
        rc.broadcastMessageSignal(message1, message2, rc.getLocation().distanceSquaredTo(target.location));
    }

    static void receiveSignals() {
        Signal[] signals = rc.emptySignalQueue();
        for (Signal s : signals) {
            if (s.getTeam() != rc.getTeam()) continue; //TODO spying!
            MessageType type = MessageType.values()[s.getMessage()[0] / 200 / 200 / 200];
            int target = (s.getMessage()[0] / 200 / 200) % 200;
            int dx = (s.getMessage()[0] / 200) % 200 - 100 + origin.x;
            int dy = s.getMessage()[0] % 200 - 100 + origin.y;
            MapLocation location = new MapLocation(dx, dy);
            if (target != rc.getID() % 200 && target != mothership.ID % 200) continue;
            switch (type) {
                case ENEMY_ARCHON:
                    nearestEnemyArchon = location;
                    break;
                case RANGED_TARGET:
                    if (target != rc.getID() % 200) continue;
                    rangedTarget = location;
                    break;
                case SURVEY_DATA:
                    break;
            }
        }
    }

}
