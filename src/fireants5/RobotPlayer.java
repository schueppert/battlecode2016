package fireants5;

import battlecode.common.*;

import java.util.*;


public class RobotPlayer {
    //Friends
    static RobotController rc;
    static LinkedList<RobotType> buildQ = new LinkedList<RobotType>(Arrays.asList(RobotType.SCOUT, RobotType.SOLDIER, RobotType.SOLDIER));
    static boolean attack = false;
    //Enemies
    static RobotInfo[] enemiesSR;
    static RobotInfo[] enemiesAR;
    static MapLocation NElocation;
    static RobotType NEtype;
    static MapLocation nearestGoody;
    static MapLocation nearestArchon;
    static MapLocation origin;
    static int mapScale;
    //Collections
    static HashMap<Integer, HashMap> friendlyArmy = new HashMap<Integer, HashMap>();
    static HashMap<Integer, HashMap> enemyArmy = new HashMap<Integer, HashMap>();
    static HashSet<MapLocation> goodies = new HashSet<MapLocation>();
    static HashSet<MapLocation> trash = new HashSet<MapLocation>();
    //Maps, Navigation & Messaging
    static Direction[][] points = new Direction[8][8];
    static final int[] PREFERENCE = {0, 1, -1, 2, -2, 3, -3, 4};
    static Direction orientation;
    static LinkedList<Integer> outbox = new LinkedList<Integer>();


    public static void run(RobotController r) throws GameActionException {
        rc = r;
        init();
        while (true) {
            update();
            receiveSignals();
            switch (rc.getType()) {
                case ARCHON:
                    if (!rc.isCoreReady()) survey();
                    else if (range(NElocation) < 50) safeMove(bearing(NElocation).opposite());
                    if (rc.isCoreReady()) activate();
                    if (rc.isCoreReady()) build();
                    if (rc.isCoreReady()) {
                        if (goodies.size() > 0 && rc.getRoundNum() < 1500) move(nearestGoody);
                        else move(NElocation);
                    }
                    repair();
                    if (rc.senseNearbyRobots(rc.getType().sensorRadiusSquared, rc.getTeam()).length > 10) sendAttack();
                    if (enemiesSR.length > 0) sendDefend();
                    if (Clock.getBytecodesLeft() > 2000) sendSignals();
                    break;
                case SCOUT:
                    if (!rc.isCoreReady()) survey();
                    else if (rc.isInfected() || range(NElocation) > mapScale) safeMove(bearing(NElocation));
                    else if (enemiesSR.length > 0) safeMove(bearing(NElocation).opposite());
                    else safeMove(orientation);
                    if (Clock.getBytecodesLeft() > 2000) sendSignals();
                    break;
                case TURRET:
                    if (rc.isWeaponReady() && enemiesAR.length > 0) attack();
                    if (rc.isWeaponReady() && NElocation != null && rc.canAttackLocation(NElocation)) rc.attackLocation(NElocation);
                    if (range(NElocation) > RobotType.TURRET.attackRadiusSquared) rc.pack();
                    break;
                case TTM:
                    if (rc.isCoreReady()) {
                        if (range(NElocation) <= RobotType.TURRET.attackRadiusSquared) rc.unpack();
                        else safeMove(bearing(NElocation));
                    }
                    break;
                default:
                    if (rc.isCoreReady()) kite(bearing(NElocation));
                    if (rc.isWeaponReady() && enemiesAR.length > 0) attack();
                    if (rc.isCoreReady()) {
                        if (attack) move(NElocation);
                        else moveCollared(nearestArchon);
                    }
                    break;
            }
            setIndicators();
            Clock.yield();
        }
    }

    //region Initialization and Updating
    static void setIndicators() {
        String string0 = (NElocation == null) ? "null" : NElocation.toString();
        String string1 = (nearestGoody == null) ? "null" : nearestGoody.toString();
        String string2 = (goodies == null) ? "null" : Integer.toString(goodies.size());
        rc.setIndicatorString(0, "Nearest Enemy: " + string0);
        rc.setIndicatorString(1, "Nearest Goody: " + string1);
        rc.setIndicatorString(2, "Goody Count: " + string2);
    }

    static void init() throws GameActionException {
        makePoints();
        orientation = points[0][rc.getID() % 8];
        origin = rallypoint(rc.getTeam());
        mapScale = origin.distanceSquaredTo(rallypoint(rc.getTeam().opponent()));
        for (RobotInfo f : rc.senseNearbyRobots(5, rc.getTeam())) {
            if (f.type == RobotType.ARCHON) putUnit(f);
        }
    }

    static void update() throws GameActionException {
        enemiesAR = rc.senseHostileRobots(rc.getLocation(), rc.getType().attackRadiusSquared);
        enemiesSR = rc.senseHostileRobots(rc.getLocation(), rc.getType().sensorRadiusSquared);
        if (rc.getType() == RobotType.ARCHON && goodies.contains(rc.getLocation())) {
            goodies.remove(rc.getLocation());
            trash.add(rc.getLocation());
            sendTrash(rc.getLocation());
        }
        nearestArchon = superlative(Superlative.NEAREST, RobotType.ARCHON, rc.getTeam());
        if (nearestArchon==null) nearestArchon=origin;
        nearestEnemy();
        nearestGoody();
    }

    static final RobotType[] keyUnits = {RobotType.ZOMBIEDEN, RobotType.ARCHON, RobotType.TURRET};

    static void nearestEnemy() throws GameActionException {
        NElocation = null;
        NEtype = null;
        if (enemiesSR.length > 0) {
            NElocation = enemiesSR[0].location;
            for (RobotInfo e : enemiesSR) {
                if (e.type == RobotType.SCOUT) continue;
                if (Arrays.asList(keyUnits).contains(e.type)) putUnit(e);
                if (rc.getType() == RobotType.SCOUT || rc.getType() == RobotType.ARCHON) sendUnit(e);
                if (range(e.location) < range(NElocation))
                    NElocation = e.location;
                NEtype = e.type;
            }
        }
        for (RobotType type : keyUnits) {
            MapLocation nearestKeyUnit = superlative(Superlative.NEAREST, type, rc.getTeam().opponent());
            if (nearestKeyUnit == null) continue;
            if (NElocation == null || range(nearestKeyUnit) < range(NElocation)) {
                NElocation = nearestKeyUnit;
                NEtype = type;
            }
        }
        if (NElocation == null) {
            NElocation = rallypoint(rc.getTeam().opponent());
            NEtype = RobotType.ARCHON;
        }
    }

    static void nearestGoody() {
        nearestGoody = null;
        if (goodies.size() > 0) {
            nearestGoody = goodies.iterator().next();
            for (MapLocation g : goodies) {
                if (nearestGoody == null || range(g) < range(nearestGoody)) nearestGoody = g;
            }
        } else nearestGoody = rallypoint(rc.getTeam());
    }
    //endregion

    //region Actions
    static void attack() throws GameActionException {
        RobotInfo weakestEnemy = enemiesAR[0];
        for (RobotInfo e : enemiesAR) {
            putUnit(e);
            if (e.health < weakestEnemy.health) weakestEnemy = e;
        }
        if (rc.isWeaponReady() && rc.canAttackLocation(weakestEnemy.location)) rc.attackLocation(weakestEnemy.location);
    }

    static void build() throws GameActionException {
        if (buildQ.size() > 0) safeBuild(buildQ.pop());
        else safeBuild(RobotType.SOLDIER);
    }

    static void safeBuild(RobotType type) throws GameActionException {
        Direction target = points[0][rc.getID() % 8];
        if (target.ordinal() > 8) return;
        for (Direction d : points[target.ordinal()])
            if (rc.canBuild(d, type) && rc.isCoreReady()) {
                rc.build(d, type);
                break;
            }
    }

    static void repair() throws GameActionException {
        for (RobotInfo f : rc.senseNearbyRobots(rc.getType().sensorRadiusSquared, rc.getTeam())) {
            if (f.type == RobotType.ARCHON) continue;
            if (f.health == f.type.maxHealth) continue;
            orientation = rc.getLocation().directionTo(f.location);
            if (rc.getLocation().distanceSquaredTo(f.location) < rc.getType().attackRadiusSquared)
                rc.repair(f.location);
            break;
        }
    }

    static void activate() throws GameActionException {
        if (goodies.contains(rc.getLocation())) {
            goodies.remove(rc.getLocation());
            trash.add(rc.getLocation());
            sendTrash(rc.getLocation());
        }
        RobotInfo[] neutrals = rc.senseNearbyRobots(2, Team.NEUTRAL);
        if (neutrals.length > 0) {
            MapLocation location = neutrals[0].location;
            rc.activate(location);
            if (goodies.contains(location)) {
                goodies.remove(location);
                trash.add(location);
                sendTrash(location);
            }
        }
    }

    static void survey() throws GameActionException {
        MapLocation[] parts = rc.sensePartLocations(rc.getType().sensorRadiusSquared);
        if (parts.length > 0) {
            for (MapLocation p : parts) {
                if (goodies.contains(p)) continue;
                double rubble = rc.senseRubble(p);
                if (rubble > 1000000) continue;
                goodies.add(p);
                sendGoodies(p);
            }
        }
        RobotInfo[] neutrals = rc.senseNearbyRobots(rc.getType().sensorRadiusSquared, Team.NEUTRAL);
        if (neutrals.length > 0) {
            for (RobotInfo r : neutrals) {
                if (goodies.contains(r.location)) continue;
                goodies.add(r.location);
                sendGoodies(r.location);
            }
        }
        if (enemyArmy.containsKey(RobotType.ZOMBIEDEN.ordinal())) {
            Collection<MapLocation> zombieDens = enemyArmy.get(RobotType.ZOMBIEDEN.ordinal()).values();
            for (MapLocation zd : zombieDens) {
                if (rc.canSense(zd)) {
                    RobotInfo r = rc.senseRobotAtLocation(zd);
                    if (r == null || r.type != RobotType.ZOMBIEDEN) {
                        trash.add(zd);
                        sendTrash(zd);
                    }
                }
            }
        }
    }
    //endregion

    //region Movement
    static void move(MapLocation target) throws GameActionException {
        if (target == null) move(orientation);
        else move(bearing(target));
    }

    static void moveCollared(MapLocation target) throws GameActionException {
        if (range(target) > 5) move(bearing(target));
        else move(bearing(target).opposite());
    }

    static void move(Direction target) throws GameActionException {
        if (enemiesSR.length > 0 && NEtype == RobotType.ZOMBIEDEN && range(NElocation) > 4) clearRubble(target);
        else if (enemiesSR.length > 0) if (rc.getType().attackRadiusSquared < NEtype.attackRadiusSquared) combatMove(target.opposite());
        else combatMove(target);
        else clearRubble(target);
    }

    static void kite(Direction target) throws GameActionException {
        if (enemiesSR.length > 0) if (rc.getType().attackRadiusSquared > NEtype.attackRadiusSquared)
            if (range(NElocation) <= NEtype.attackRadiusSquared)
                combatMove(target.opposite());
    }

    static void safeMove(Direction target) throws GameActionException {
        if (target.ordinal() > 7) return;
        for (Direction d : points[target.ordinal()]) {
            if (turretCovered(rc.getLocation().add(d))) continue;
            if (rc.canMove(d) && rc.isCoreReady() && Clock.getBytecodesLeft() > 500) {
                orientation = d;
                rc.move(d);
                break;
            }
        }
    }

    static void clearRubble(Direction target) throws GameActionException {
        Direction[] keyMoves = new Direction[]{target, target.rotateLeft(), target.rotateRight()};
        for (Direction d : keyMoves) {
            if (rc.canMove(d) && rc.isCoreReady()) {
                rc.move(d);
                return;
            }
        }
        Direction bestPath = target;
        double lowestRubble = 1000000;
        for (Direction d : keyMoves) {
            if (rc.canSense(rc.getLocation().add(d))) {
                double rubble = rc.senseRubble(rc.getLocation().add(d));
                if (rubble < lowestRubble) {
                    lowestRubble = rubble;
                    bestPath = d;
                }
            }
        }
        if (rc.onTheMap(rc.getLocation().add(bestPath))) rc.clearRubble(bestPath);
        else safeMove(target);
    }

    static boolean turretCovered(MapLocation location) {
        if (enemyArmy.containsKey(RobotType.TURRET.ordinal()))
            for (MapLocation turret : (Collection<MapLocation>) enemyArmy.get(RobotType.TURRET.ordinal()).values()) {
                if (location.distanceSquaredTo(turret) <= RobotType.TURRET.attackRadiusSquared)
                    return true;
            }
        return false;
    }

    static void combatMove(Direction target) throws GameActionException {
        Direction bestMove = Direction.NONE;
        int bestScore = combatScore(bestMove);
        for (Direction d : points[target.ordinal()]) {
            if (!rc.canMove(d)) continue;
            int score = combatScore(d);
            if (score < bestScore) {
                bestScore = score;
                bestMove = d;
            }
        }
        if (bestMove != Direction.NONE) safeMove(bestMove);
    }

    static int combatScore(Direction d) {
        int score = 0;
        MapLocation newLocation = rc.getLocation().add(d.dx, d.dy);
        for (RobotInfo e : enemiesSR) {
            if (e.type == RobotType.ARCHON || e.type == RobotType.SCOUT) continue;
            if (newLocation.distanceSquaredTo(e.location) <= e.type.attackRadiusSquared) score += 1;
        }
        return score;
    }

    static MapLocation rallypoint(Team team) {
        HashMap<Integer, HashMap> army = (team == rc.getTeam()) ? friendlyArmy : enemyArmy;
        Collection<MapLocation> archons;
        if (army.containsKey(RobotType.ARCHON.ordinal())) archons = army.get(RobotType.ARCHON.ordinal()).values();
        else archons = Arrays.asList(rc.getInitialArchonLocations(team));
        int dx = 0;
        int dy = 0;
        for (MapLocation archon : archons) {
            dx += archon.x;
            dy += archon.y;
        }
        dx /= archons.size();
        dy /= archons.size();
        return new MapLocation(dx, dy);
    }

    enum Superlative {
        NEAREST, FARTHEST
    }

    static MapLocation superlative(Superlative s, RobotType type, Team team) throws GameActionException {
        Collection<MapLocation> candidates = null;
        HashMap<Integer, HashMap> army = (team == rc.getTeam()) ? friendlyArmy : enemyArmy;
        if (army.containsKey(type.ordinal())) candidates = army.get(type.ordinal()).values();
        if (type == RobotType.ARCHON && (candidates == null || rc.getRoundNum() < 5))
            candidates = Arrays.asList(rc.getInitialArchonLocations(team));
        if (candidates == null) return null;
        MapLocation superlative = null;
        for (MapLocation c : candidates) {
            if (trash.contains(c)) continue;
            if (rc.canSense(c)) {
                RobotInfo r = rc.senseRobotAtLocation(c);
                if (r == null || r.type != type) trash.add(c);
                continue;
            }
            if (superlative == null) superlative = c;
            else switch (s) {
                case NEAREST:
                    if (superlative == null || range(c) < range(superlative)) superlative = c;
                    break;
                case FARTHEST:
                    if (superlative == null || range(c) > range(superlative)) superlative = c;
                    break;
            }
        }
        return superlative;
    }

    static void makePoints() {
        Direction[] directions = Arrays.copyOfRange(Direction.values(), 0, 8);
        int handedness = (rc.getID() % 2 == 0) ? 1 : -1;
        for (int i = 0; i < directions.length; i++)
            for (int j = 0; j < PREFERENCE.length; j++) points[i][j] = directions[(i + PREFERENCE[j] * handedness + 8) % 8];
    }

    static int range(MapLocation location) {
        return rc.getLocation().distanceSquaredTo(location);
    }

    static Direction bearing(MapLocation location) {
        return rc.getLocation().directionTo(location);
    }
//endregion

    //region Messaging
    enum MessageType {
        EMPTY, ENEMY_UNIT, FRIENDLY_UNIT, GOODIES, TRASH, ATTACK, DEFEND;
    }

    static void sendSignals() throws GameActionException {
        putUnit(rc.senseRobot(rc.getID()));
        sendUnit(rc.senseRobot(rc.getID()));
        int range = 2 * rc.getType().sensorRadiusSquared;
        if ((rc.getRoundNum() + rc.getID()) % 20 == 0) {
            rebroadcast(rc.getTeam().opponent(), RobotType.TURRET);
            rebroadcast(rc.getTeam().opponent(), RobotType.ZOMBIEDEN);
            rebroadcast(rc.getTeam().opponent(), RobotType.ARCHON);
            rebroadcast(rc.getTeam(), RobotType.ARCHON);
            for (MapLocation location : goodies) sendGoodies(location);
            for (MapLocation location : trash) sendTrash(location);
        }
        while (outbox.size() > 1 && rc.getMessageSignalCount() < GameConstants.MESSAGE_SIGNALS_PER_TURN) {
            rc.broadcastMessageSignal(outbox.pop(), outbox.pop(), range);
        }
        if (outbox.size() > 0 && rc.getMessageSignalCount() < GameConstants.MESSAGE_SIGNALS_PER_TURN) {
            rc.broadcastMessageSignal(outbox.pop(), 0, range);
        }
    }

    static void rebroadcast(Team team, RobotType type) throws GameActionException {
        HashMap<Integer, HashMap> army = (team == rc.getTeam()) ? friendlyArmy : enemyArmy;
        MessageType messageType = (team == rc.getTeam()) ? MessageType.FRIENDLY_UNIT : MessageType.ENEMY_UNIT;
        if (army.containsKey(type.ordinal())) {
            HashMap units = army.get(type.ordinal());
            for (int id : (Collection<Integer>) units.keySet())
                sendMessage(id, (MapLocation) units.get(id), type.ordinal(), messageType);
        }
    }

    static void sendUnit(RobotInfo e) throws GameActionException {
        MessageType messageType = (e.team == rc.getTeam()) ? MessageType.FRIENDLY_UNIT : MessageType.ENEMY_UNIT;
        sendMessage(e.ID % 256, e.location, e.type.ordinal(), messageType);
    }

    static void sendGoodies(MapLocation location) throws GameActionException {
        sendMessage(0, location, 0, MessageType.GOODIES);
    }

    static void sendTrash(MapLocation location) throws GameActionException {
        sendMessage(0, location, 0, MessageType.TRASH);
    }

    static void sendAttack() throws GameActionException {
        sendMessage(0, rc.getLocation(), 0, MessageType.ATTACK);
    }

    static void sendDefend() throws GameActionException {
        sendMessage(0, rc.getLocation(), 0, MessageType.DEFEND);
    }

    static void sendMessage(int id, MapLocation location, int unitType, MessageType messageType) throws GameActionException {
        location = translate(location);
        int message = (id % 256) * 256 * 256 * 128;
        message += location.x * 256 * 128;
        message += location.y * 128;
        message += unitType * 8;
        message += messageType.ordinal();
        outbox.push(message);
    }

    static void receiveSignals() {
        Signal[] signals = rc.emptySignalQueue();
        for (Signal signal : signals) {
            if (signal.getTeam() != rc.getTeam()) continue; //TODO spying!
            if (signal.getMessage() == null) continue;//TODO BeastMode!
            for (int message : signal.getMessage()) {
                int id = message / 256 / 256 / 128;
                int dx = (message / 256 / 128) % 256;
                int dy = (message / 128) % 256;
                MapLocation location = untranslate(new MapLocation(dx, dy));
                int unitType = (message / 8) % 16;
                switch (MessageType.values()[message % 8]) {
                    case EMPTY:
                        break;
                    case ENEMY_UNIT:
                        putUnit(id, location, unitType, rc.getTeam().opponent());
                        if (NElocation == null || range(location) < range(NElocation)) {
                            NElocation = location;
                            NEtype = RobotType.values()[unitType];
                        }
                        break;
                    case FRIENDLY_UNIT:
                        putUnit(id, location, unitType, rc.getTeam());
                        break;
                    case GOODIES:
                        if (!trash.contains(location)) goodies.add(location);
                        break;
                    case TRASH:
                        if (goodies.contains(location)) goodies.remove(location);
                        trash.add(location);
                        break;
                    case ATTACK:
                        attack = true;
                        break;
                    case DEFEND:
                        attack = false;
                }
            }
        }
    }

    static void putUnit(RobotInfo e) {
        putUnit(e.ID % 256, e.location, e.type.ordinal(), e.team);
    }

    static void putUnit(int id, MapLocation location, int type, Team team) {
        HashMap<Integer, HashMap> army = (team == rc.getTeam()) ? friendlyArmy : enemyArmy;
        HashMap units = (army.containsKey(type)) ? army.get(type) : new HashMap<Integer, MapLocation>();
        units.put(id, location);
        army.put(type, units);
    }

    static MapLocation translate(MapLocation location) {
        return new MapLocation(location.x - origin.x + 100, location.y - origin.y + 100);
    }

    static MapLocation untranslate(MapLocation location) {
        return new MapLocation(location.x + origin.x - 100, location.y + origin.y - 100);
    }
    //endregion

}
