package seeding;

import battlecode.common.*;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;


public class RobotPlayer {
    //Friends
    static RobotController rc;
    static final int ALAMO = 400;
    static LinkedList<RobotType> buildQ = new LinkedList<RobotType>(Arrays.asList(RobotType.SCOUT, RobotType.SOLDIER, RobotType.SOLDIER));
    static double soldierCount;
    //Enemies
    static RobotInfo[] enemiesSR;
    static RobotInfo[] enemiesAR;
    static MapLocation rangedTarget;
    static MapLocation rallyPoint;
    static MapLocation nearestParts;
    static MapLocation origin;
    //Collections
    static HashMap<Integer, HashMap> friendlyArmy = new HashMap<Integer, HashMap>();
    static HashMap<Integer, HashMap> enemyArmy = new HashMap<Integer, HashMap>();
    static HashMap<MapLocation, Integer> parts = new HashMap<MapLocation, Integer>();
    //Maps and Navigation
    static Direction[][] points = new Direction[8][8];
    static final int[] PREFERENCE = {0, 1, -1, 2, -2, 3, -3, 4};
    static Direction orientation;
    //Messaging
    static LinkedList<Integer> outbox = new LinkedList<Integer>();


    public static void run(RobotController r) throws GameActionException {
        rc = r;
        init();
        while (true) {
            update();
            receiveSignals();
            switch (rc.getType()) {
                case ARCHON:
                    if (!rc.isCoreReady()) {
                        surveyParts();
                        break;
                    }
                    if (enemiesSR.length > 0) move(rallyPoint);
                    else build();
                    if (!rc.isCoreReady()) break;
                    if (range(rallyPoint) < RobotType.ARCHON.sensorRadiusSquared) {
                        MapLocation nearestZD = superlative(Superlative.NEAREST, RobotType.ZOMBIEDEN, rc.getTeam().opponent());
                        if (nearestParts == rallyPoint && nearestZD != null) move(rc.getLocation().directionTo(nearestZD).opposite());
                        else move(nearestParts);
                    } else move(rallyPoint);
                    repair();
                    break;
                case SCOUT:
                    if (!rc.isCoreReady()) {
                        surveyParts();
                        break;
                    } else if (rc.isInfected()) move(superlative(Superlative.NEAREST, RobotType.ARCHON, rc.getTeam().opponent()));
                    else if (rc.getRoundNum() > ALAMO) move(rallyPoint);
                    else move(orientation);
                    break;
                case SOLDIER:
                    if (rc.isWeaponReady() && enemiesAR.length > 0) attack();
                    if (rc.isCoreReady()) move(rallyPoint);
                    break;
                case TURRET:
                    if (rc.isWeaponReady()) {
                        if (enemiesAR.length > 0) attack();
                        else if (rangedTarget != null && rc.canAttackLocation(rangedTarget))
                            rc.attackLocation(rangedTarget);
                    }
                    if (!rc.isCoreReady()) break;
                    MapLocation nearestArchon = superlative(Superlative.NEAREST, RobotType.ARCHON, rc.getTeam());
                    int range = range(nearestArchon);
                    if (range > rc.getType().sensorRadiusSquared + 10 || range < 3)
                        rc.pack();
                    break;
                case TTM:
                    if (!rc.isCoreReady()) break;
                    nearestArchon = superlative(Superlative.NEAREST, RobotType.ARCHON, rc.getTeam());
                    range = range(nearestArchon);
                    if (range < rc.getType().sensorRadiusSquared - 10 && range > 2)
                        rc.unpack();
                    else {
                        if (range > 2) move(nearestArchon);
                        else move(rc.getLocation().directionTo(nearestArchon).opposite());
                    }
            }
            if (Clock.getBytecodesLeft() > 2000) sendMessages();
            setIndicators();
            Clock.yield();
        }
    }

    //region Initialization and Updating
    static void setIndicators() {
        String archons = (friendlyArmy.containsKey(RobotType.ARCHON.ordinal())) ? friendlyArmy.get(RobotType.ARCHON.ordinal()).values().toString() : "null";
        int zdCount = (enemyArmy.containsKey(RobotType.ZOMBIEDEN.ordinal())) ? enemyArmy.get(RobotType.ZOMBIEDEN.ordinal()).size() : 0;
        rc.setIndicatorString(0, "Rallypoint: " + rallyPoint.toString());
        rc.setIndicatorString(1, "Nearest Parts: " + nearestParts.toString());
        rc.setIndicatorString(2, "Parts Count: " + Integer.toString(parts.keySet().size()) + "  ZD Count: " + Integer.toString(zdCount));
    }

    static void init() throws GameActionException {
        makePoints();
        orientation = points[0][rc.getID() % 8];
        origin = rallyPoint(rc.getTeam());
        rallyPoint = origin;
        nearestParts = origin;
        if (rc.getType() == RobotType.ARCHON) {
            putUnit(rc.senseRobot(rc.getID()));
            sendUnit(rc.senseRobot(rc.getID()));
        }
    }

    static void update() throws GameActionException {
        rangedTarget = null;
        enemiesAR = rc.senseHostileRobots(rc.getLocation(), rc.getType().attackRadiusSquared);
        enemiesSR = rc.senseHostileRobots(rc.getLocation(), rc.getType().sensorRadiusSquared);
        soldierCount -= soldierCount / 20;
        RobotInfo[] friends = rc.senseNearbyRobots(rc.getType().sensorRadiusSquared, rc.getTeam());
        if (friends.length > 0) for (RobotInfo f : friends) if (f.type == RobotType.ARCHON) putUnit(f);
        rallyPoint = rallyPoint(rc.getTeam());
        if (rc.getType() == RobotType.ARCHON) nearestParts = nearestParts();
    }
    //endregion

    //region Actions
    static void attack() throws GameActionException {
        RobotInfo weakestEnemy = enemiesAR[0];
        for (RobotInfo e : enemiesAR) {
            putUnit(e);
            if (e.health < weakestEnemy.health) weakestEnemy = e;
        }
        if (rc.isWeaponReady() && rc.canAttackLocation(weakestEnemy.location)) rc.attackLocation(weakestEnemy.location);
    }

    static void build() throws GameActionException {
        if (buildQ.size() > 0) safeBuild(buildQ.pop());
        else if (soldierCount < 20) safeBuild(RobotType.SOLDIER);
        else safeBuild(RobotType.TURRET);

    }

    static void safeBuild(RobotType type) throws GameActionException {
        Direction target = points[0][rc.getID() % 8];
        if (target.ordinal() > 8) return;
        for (Direction d : points[target.ordinal()])
            if (rc.canBuild(d, type) && rc.isCoreReady()) {
                rc.build(d, type);
                break;
            }
    }

    static void repair() throws GameActionException {
        for (RobotInfo f : rc.senseNearbyRobots(rc.getType().sensorRadiusSquared, rc.getTeam())) {
            if (f.type == RobotType.ARCHON) continue;
            if (f.health == f.type.maxHealth) continue;
            orientation = rc.getLocation().directionTo(f.location);
            if (rc.getLocation().distanceSquaredTo(f.location) < rc.getType().attackRadiusSquared)
                rc.repair(f.location);
            break;
        }
    }
    //endregion

    //region Movement
    static void move(MapLocation target) throws GameActionException {
        move(rc.getLocation().directionTo(target));
    }

    static void move(Direction target) throws GameActionException {
        if (enemiesSR.length == 1 && enemiesSR[0].type == RobotType.ZOMBIEDEN) safeMove(target);
        if (enemiesSR.length > 0) combatMove();
        else if (rc.getType() == RobotType.SOLDIER) {
            MapLocation nearestArchon = superlative(Superlative.NEAREST, RobotType.ARCHON, rc.getTeam());
            if (range(nearestArchon) < 10) clearRubble((rc.getLocation().directionTo(nearestArchon).opposite()));
            else clearRubble(target);
        } else safeMove(target);
    }

    static void safeMove(Direction target) throws GameActionException {
        if (target.ordinal() > 7) return;
        for (Direction d : points[target.ordinal()]) {
            if (turretCovered(rc.getLocation().add(d))) continue;
            if (rc.canMove(d) && rc.isCoreReady() && Clock.getBytecodesLeft() > 500) {
                orientation = d;
                rc.move(d);
                break;
            }
        }
    }

    static MapLocation rallyPoint(Team team) {
        HashMap<Integer, HashMap> army = (team == rc.getTeam()) ? friendlyArmy : enemyArmy;
        Collection<MapLocation> archons;
        if (army.containsKey(RobotType.ARCHON.ordinal())) archons = army.get(RobotType.ARCHON.ordinal()).values();
        else archons = Arrays.asList(rc.getInitialArchonLocations(team));
        int dx = 0;
        int dy = 0;
        for (MapLocation archon : archons) {
            dx += archon.x;
            dy += archon.y;
        }
        dx /= archons.size();
        dy /= archons.size();
        rallyPoint = new MapLocation(dx, dy);
        return rallyPoint;
    }

    static MapLocation nearestParts() throws GameActionException {
        nearestParts = null;
        int nearestRange = 100000;
        MapLocation[] closeParts = rc.sensePartLocations(rc.getType().sensorRadiusSquared);
        if (closeParts.length > 0) {
            for (MapLocation p : closeParts) {
                double rubble = rc.senseRubble(p);
                if (rubble >= 100) continue;
                int range = rallyPoint.distanceSquaredTo(p);
                if (range < nearestRange) {
                    nearestRange = range;
                    nearestParts = p;
                }
            }
        }
        if (parts.keySet().size() > 0) {
            for (MapLocation p : parts.keySet()) {
                int range = rallyPoint.distanceSquaredTo(p);
                if (range < nearestRange) {
                    nearestRange = range;
                    nearestParts = p;
                }
            }
        }
        if (nearestParts != null) return nearestParts;
        return rallyPoint;
    }

    enum Superlative {NEAREST, FARTHEST}

    static MapLocation superlative(Superlative s, RobotType type, Team team) {
        Collection<MapLocation> candidates = null;
        HashMap<Integer, HashMap> army = (team == rc.getTeam()) ? friendlyArmy : enemyArmy;
        if (army.containsKey(type.ordinal())) candidates = army.get(type.ordinal()).values();
        if (type == RobotType.ARCHON && (candidates == null || rc.getRoundNum() < 5))
            candidates = Arrays.asList(rc.getInitialArchonLocations(team));
        if (candidates == null) return null;
        MapLocation superlative = candidates.iterator().next();
        for (MapLocation c : candidates) {
            switch (s) {
                case NEAREST:
                    if (rc.getLocation().distanceSquaredTo(c) < rc.getLocation().distanceSquaredTo(superlative)) superlative = c;
                    break;
                case FARTHEST:
                    if (rc.getLocation().distanceSquaredTo(c) > rc.getLocation().distanceSquaredTo(superlative)) superlative = c;
                    break;
            }
        }
        return superlative;
    }

    static boolean turretCovered(MapLocation location) {
        if (enemyArmy.containsKey(RobotType.TURRET.ordinal()))
            for (MapLocation turret : (Collection<MapLocation>) enemyArmy.get(RobotType.TURRET.ordinal()).values()) {
                if (location.distanceSquaredTo(turret) <= RobotType.TURRET.attackRadiusSquared)
                    return true;
            }
        return false;
    }

    static void combatMove() throws GameActionException {
        RobotInfo nearestEnemy = enemiesSR[0];
        for (RobotInfo e : enemiesSR) {
            putUnit(e);
            if (rc.getType() == RobotType.SCOUT || rc.getType() == RobotType.ARCHON) sendUnit(e);
            if (rc.getLocation().distanceSquaredTo(e.location) < rc.getLocation().distanceSquaredTo(nearestEnemy.location))
                nearestEnemy = e;
        }
        Direction target = rc.getLocation().directionTo(nearestEnemy.location);
        if (rc.getType() == RobotType.ARCHON || rc.getType() == RobotType.SCOUT) {
            safeMove(target.opposite().rotateRight());
            return;
        }
        if (combatScore(target) > 0) target = target.opposite();
        Direction bestMove = Direction.NONE;
        int bestScore = combatScore(bestMove);
        for (Direction d : points[target.ordinal()]) {
            if (!rc.canMove(d)) continue;
            int score = combatScore(d);
            if (score < bestScore) {
                bestScore = score;
                bestMove = d;
            }
        }
        safeMove(bestMove);
    }

    static int combatScore(Direction d) {
        int score = 0;
        MapLocation newLocation = rc.getLocation().add(d.dx, d.dy);
        for (RobotInfo e : enemiesSR) {
            if (e.type == RobotType.ARCHON || e.type == RobotType.SCOUT) continue;
            if (newLocation.distanceSquaredTo(e.location) <= e.type.attackRadiusSquared) score += 1;
        }
        return score;
    }

    static void makePoints() {
        Direction[] directions = Arrays.copyOfRange(Direction.values(), 0, 8);
        int handedness = (rc.getID() % 2 == 0) ? 1 : -1;
        for (int i = 0; i < directions.length; i++)
            for (int j = 0; j < PREFERENCE.length; j++) points[i][j] = directions[(i + PREFERENCE[j] * handedness + 8) % 8];
    }

    static int range(MapLocation location) {
        return rc.getLocation().distanceSquaredTo(location);
    }

    static void clearRubble(Direction target) throws GameActionException {
        if (target == Direction.NONE) target = rc.getLocation().directionTo(rallyPoint).opposite();
        Direction[] keyMoves = new Direction[]{target, target.rotateLeft(), target.rotateRight()};
        for (Direction d : keyMoves) {
            if (rc.canMove(d) && rc.isCoreReady()) {
                rc.move(d);
                return;
            }
        }
        for (Direction d : keyMoves) {
            if (rc.canSense(rc.getLocation().add(d))) {
                double rubble = rc.senseRubble(rc.getLocation().add(d));
                if (rubble >= 100 && rubble <= 1000) {
                    rc.clearRubble(d);
                    return;
                }
            }
        }
        safeMove(target);
    }

    static void surveyParts() {
        MapLocation[] parts = rc.sensePartLocations(rc.getType().sensorRadiusSquared);
        for (MapLocation p : parts) {
            if (RobotPlayer.parts.containsKey(p)) continue;
            double rubble = rc.senseRubble(p);
            if (rubble >= 100) continue;
            int quantity = (int) rc.senseParts(p);
            RobotPlayer.parts.put(p, quantity);
            sendParts(MessageType.PARTS_ADD, p, quantity);
        }
    }
    //endregion

    //region Messaging
    enum MessageType {
        EMPTY, ENEMY_UNIT, FRIENDLY_UNIT, PARTS_ADD, PARTS_REMOVE, REMOVE_ZD
    }

    static MapLocation translate(MapLocation location) {
        return new MapLocation(location.x - origin.x + 100, location.y - origin.y + 100);
    }

    static MapLocation untranslate(MapLocation location) {
        return new MapLocation(location.x + origin.x - 100, location.y + origin.y - 100);
    }

    static void sendMessages() throws GameActionException {
        int range;
        if (rc.getRoundNum() < 2)
            range = rc.getLocation().distanceSquaredTo(superlative(Superlative.FARTHEST, RobotType.ARCHON, rc.getTeam()));
        else range = 2 * rc.getType().sensorRadiusSquared;
        if (rc.getType() == RobotType.ARCHON) {
            sendParts(MessageType.PARTS_REMOVE, rc.getLocation(), 0);
            if (parts.containsKey(rc.getLocation())) parts.remove(rc.getLocation());
        }
        if ((rc.getRoundNum() + rc.getID()) % 20 == 0 && rc.getType() == RobotType.SOLDIER) rc.broadcastSignal(range);
        if ((rc.getRoundNum() + rc.getID()) % 20 == 0 && (rc.getType() == RobotType.ARCHON || rc.getType() == RobotType.SCOUT)) {
            rebroadcast(rc.getTeam().opponent(), RobotType.TURRET);
            rebroadcast(rc.getTeam(), RobotType.ARCHON);
            for (MapLocation p : parts.keySet()) {
                sendParts(MessageType.PARTS_ADD, p, parts.get(p));
            }
        }
        while (outbox.size() > 1 && rc.getMessageSignalCount() < GameConstants.MESSAGE_SIGNALS_PER_TURN) {
            rc.broadcastMessageSignal(outbox.pop(), outbox.pop(), range);
        }
        if (outbox.size() > 0 && rc.getMessageSignalCount() < GameConstants.MESSAGE_SIGNALS_PER_TURN) {
            rc.broadcastMessageSignal(outbox.pop(), 0, range);
        }
    }

    static void rebroadcast(Team team, RobotType type) throws GameActionException {
        HashMap<Integer, HashMap> army = (team == rc.getTeam()) ? friendlyArmy : enemyArmy;
        if (army.containsKey(type.ordinal())) {
            HashMap units = army.get(type.ordinal());
            for (int id : (Collection<Integer>) units.keySet())
                sendUnit(id, (MapLocation) units.get(id), type.ordinal(), team);
        }
    }

    static void sendUnit(RobotInfo e) throws GameActionException {
        sendUnit(e.ID % 256, e.location, e.type.ordinal(), e.team);
    }

    static void putUnit(RobotInfo e) {
        putUnit(e.ID % 256, e.location, e.type.ordinal(), e.team);
    }

    static void sendUnit(int id, MapLocation location, int type, Team team) throws GameActionException {
        location = translate(location);
        int message = (id % 256) * 256 * 256 * 128;
        message += location.x * 256 * 128;
        message += location.y * 128;
        message += type * 8;
        if (team == rc.getTeam()) message += MessageType.FRIENDLY_UNIT.ordinal();
        else message += MessageType.ENEMY_UNIT.ordinal();
        outbox.push(message);
    }

    static void sendParts(MessageType type, MapLocation location, int quantity) {
        location = translate(location);
        int message = Math.min(200, quantity) * 256 * 256 * 8;
        message += location.x * 256 * 8;
        message += location.y * 8;
        message += type.ordinal();
        outbox.addLast(message);
    }

    static void addParts(int message) {
        int quantity = message / 256 / 256 / 8;
        int dx = (message / 256 / 8) % 256;
        int dy = (message / 8) % 256;
        MapLocation location = untranslate(new MapLocation(dx, dy));
        parts.put(location, quantity);
    }

    static void removeParts(int message) {
        int quantity = message / 256 / 256 / 8;
        int dx = (message / 256 / 8) % 256;
        int dy = (message / 8) % 256;
        MapLocation location = untranslate(new MapLocation(dx, dy));
        if (parts.containsKey(location)) parts.remove(location);
    }

    static void removeZD(MapLocation location) {
        if (rc.getType() != RobotType.ARCHON && rc.getType() != RobotType.SCOUT) return;
        location = translate(location);
        int message = location.x * 256 * 8;
        message += location.y * 8;
        message += MessageType.REMOVE_ZD.ordinal();
        outbox.push(message);
        removeZD(message);
    }

    static void removeZD(int message) {
        int dx = (message / 256 / 8) % 256;
        int dy = (message / 8) % 256;
        MapLocation location = untranslate(new MapLocation(dx, dy));
        if (!enemyArmy.containsKey(RobotType.ZOMBIEDEN.ordinal())) return;
        HashMap zombieDens = enemyArmy.get(RobotType.ZOMBIEDEN.ordinal());
        Collection<Integer> idSet = zombieDens.keySet();
        for (int id : idSet) if (zombieDens.get(id) == location) zombieDens.remove(id);
    }

    static void putUnit(int id, MapLocation location, int type, Team team) {
        HashMap<Integer, HashMap> army = (team == rc.getTeam()) ? friendlyArmy : enemyArmy;
        HashMap units = (army.containsKey(type)) ? army.get(type) : new HashMap<Integer, MapLocation>();
        units.put(id, location);
        army.put(type, units);
    }

    static void putMessage(Team team, int message, Signal signal) {
        int id = message / 256 / 256 / 128;
        int dx = (message / 256 / 128) % 256;
        int dy = (message / 128) % 256;
        MapLocation location = untranslate(new MapLocation(dx, dy));
        int type = (message / 8) % 16;
        putUnit(id, location, type, team);
        if (rc.getType() == RobotType.TURRET && team == rc.getTeam().opponent())
            if (rangedTarget == null || rc.getLocation().distanceSquaredTo(location) < rc.getLocation().distanceSquaredTo(rangedTarget))
                rangedTarget = location;
    }

    static void receiveSignals() {
        Signal[] signals = rc.emptySignalQueue();
        for (Signal signal : signals) {
            if (signal.getTeam() != rc.getTeam()) continue; //TODO spying!
            if (signal.getMessage() == null) soldierCount++;
            else for (int message : signal.getMessage())
                switch (MessageType.values()[message % 8]) {
                    case EMPTY:
                        break;
                    case ENEMY_UNIT:
                        putMessage(rc.getTeam().opponent(), message, signal);
                        break;
                    case FRIENDLY_UNIT:
                        putMessage(rc.getTeam(), message, signal);
                        break;
                    case PARTS_ADD:
                        addParts(message);
                        break;
                    case PARTS_REMOVE:
                        removeParts(message);
                        break;
                    case REMOVE_ZD:
                        removeZD(message);
                }
        }
    }
    //endregion

}
