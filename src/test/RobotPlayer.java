package test;

import battlecode.common.*;

/**
 * Created by schueppert on 1/13/2016.
 */
public class RobotPlayer {

    public static void run(RobotController rc) throws GameActionException {
        Direction o = Direction.values()[rc.getID() % 8];
        while (true) {
            if (rc.canMove(o) && rc.isCoreReady()) rc.move(o);
            else o = o.rotateRight();
        }
    }

}
